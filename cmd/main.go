package main

import (
	"strings"

	"github.com/spf13/viper"
	"gitlab.com/laisiacode/physicist/pkg/cmd"
	"go.uber.org/zap"
)

func main() {
	viper.AutomaticEnv()
	viper.SetEnvPrefix("PHYSICIST")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	logger, _ := zap.NewDevelopment()
	zap.ReplaceGlobals(logger)

	cmd.Execute()
}
