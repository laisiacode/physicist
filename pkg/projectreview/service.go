package projectreview

import (
	"errors"
	"fmt"

	"github.com/xanzy/go-gitlab"
)

type Service struct {
	client *gitlab.Client
}

func NewService(token, baseURL string) (*Service, error) {
	if token == "" {
		return nil, errors.New("empty gitlab token")
	}

	if baseURL == "" {
		return nil, errors.New("empty gitlab base URL")
	}

	c, err := gitlab.NewClient(
		token,
		gitlab.WithBaseURL(baseURL),
	)
	if err != nil {
		return nil, err
	}

	return &Service{c}, nil
}

func (s *Service) Execute() error {
	archived := false
	//orderby := "last_activity_at"
	mrEnabled := true

	projects, _, err := s.client.Projects.ListProjects(&gitlab.ListProjectsOptions{
		ListOptions: gitlab.ListOptions{PerPage: 100},
		Archived:    &archived,
		//OrderBy:                  &orderby,
		WithMergeRequestsEnabled: &mrEnabled,
	})
	if err != nil {
		return err
	}

	for i := range projects {
		printProjectInfo(projects[i])
	}
	return nil
}

func printProjectInfo(p *gitlab.Project) {
	fmt.Println(
		p.ID,
		p.PathWithNamespace,
		p.Description,
		p.Topics,
		p.ReadmeURL,
	)
}
