package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/laisiacode/physicist/pkg/projectreview"
	"go.uber.org/zap"
)

func init() {
	rootCmd.AddCommand(projectReviewCmd)
}

var projectReviewCmd = &cobra.Command{
	Use:   "project-review",
	Short: "review project info",
	Long:  `review project info`,
	Run: func(cmd *cobra.Command, args []string) {
		s, err := projectreview.NewService(viper.GetString("token"), viper.GetString("baseurl"))
		if err != nil {
			zap.S().Fatal(err)
		}

		err = s.Execute()
		if err != nil {
			zap.S().Fatal(err)
		}
	},
}
