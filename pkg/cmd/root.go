package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "physicist",
	Short: "physicist",
	Long:  `toolkits for do something via gitlab api`,
	Run: func(cmd *cobra.Command, args []string) {
	},
}

// Execute _
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
